import  csv
import sys

argc = len(sys.argv)
if (argc != 3):
    print("incorrect input.\n example use : python3 statistic.py /path/to/src_ file.csv /path/to/dst_file.csv")
    exit(1)

input_filepath = sys.argv[1]
output_filepath = sys.argv[2]
map_obj = {}
def sum(a:list, b:list):
    for i in range(len(b)):
        b[i] += a[i]
    return b
with open(input_filepath, newline='\n') as csvfile:
   reader = csv.reader(csvfile, delimiter=';', quotechar=None)
   for row in reader:
    src, dst , coun, size = row
    if reader.line_num != 1 :
       map_obj[str(src)] = sum([0,0,int(coun),int(size)] , map_obj.setdefault(str(src),[0,0,0,0]))
       map_obj[str(dst)] = sum([int(coun),int(size),0,0], map_obj.setdefault(str(dst),[0,0,0,0]))
with open(output_filepath, 'w', newline='\n') as output_file:
    keys = map_obj.keys()
    for item in keys:
        output = str(item)
        for v in list(map_obj[item]):
            output+= ";"+ str(v)
        output_file.writelines(output + "\n")




