**Анализатор трафика**    

Реализовано на :
    
* Ubuntu 22
* C++17

**Зависимости**: 

* PcapPlusPlus

**Сборка**:

    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=/path/to/install ../
    make install

**Запуск**:

Help :

     ./traffic_analyzer -h

Запуск в режиме чтения из файла :

        ./traffic_analyzer -f /path/to/pcap/file -o output/path/output.csv -p count_packets

Запуск в режиме чтения интерфейса

        ./traffic_analyzer -i interface_name -o output/path/output.csv -p count_packets

**Python**

Находится в папке python

Запуск:

        python3 statistic.py /path/to/src_ file.csv /path/to/dst_file.csv"

**Автор**:

    dldanildm@mail.ru   tg: dldndm


