#include "FileDevice.h"

std::unique_ptr<IDevice> FileDevice::create(std::string& path)
{
    try
    {
        return std::unique_ptr<IDevice>(new FileDevice(path));
    }
    catch (const std::exception& ex)
    {
      std::cerr << "What :" << ex.what() << std::endl;
    }
};
std::optional<pcpp::RawPacket> FileDevice::get()
{
    pcpp::RawPacket rawPacket;
    if (_reader->getNextPacket(rawPacket))
    {
        return rawPacket;
    }
    return std::nullopt;
};

std::unique_ptr<pcpp::PcapFileReaderDevice, decltype(&Device<pcpp::PcapFileReaderDevice>::deleter)> FileDevice::create_reader(std::string& name)
{
    return std::unique_ptr<pcpp::PcapFileReaderDevice, decltype(&deleter)>(new pcpp::PcapFileReaderDevice(name), &deleter);
}

FileDevice::FileDevice(std::string& path): Device<pcpp::PcapFileReaderDevice>(path,create_reader(path))
{
}


