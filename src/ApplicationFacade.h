#pragma once
#include <iostream>
#include <getopt.h>
#include <map>
#include "InStream.h"
class ApplicationFacade
{
public:
    ApplicationFacade(int argc, char* argv[]);
    void run();
private:
    void printUsage();
    void createInputStream();
    void init(int argc, char* argv[]);
    void parse_options(int argc, char* argv[]);
    void update_map(std::pair<std::string,std::string>&&,
                    std::pair<uint,uint>&&);
    static constexpr struct option options[] =
            {
                    {"interface_name",  required_argument, nullptr, 'i'},
                    {"input-file",  required_argument, nullptr, 'f'},
                    {"output-file", required_argument, nullptr, 'o'},
                    {"count_packets", required_argument, nullptr, 'p'},
                    {"help", no_argument, nullptr, 'h'},
                    {nullptr, 0, nullptr, 0}
            };
private:
    std::string _output_filepath;
    std::string _input_filepath;
    uint _count_packets;
    std::string _interface_name;
    std::unique_ptr<IStream> _input_stream;
    std::map<std::pair<std::string,std::string>,
             std::pair<uint,uint>> _statistic;

};