#include "Device.h"


class FileDevice : public Device<pcpp::PcapFileReaderDevice>
{
public:
    using FileReaderPtr =   std::unique_ptr<pcpp::PcapFileReaderDevice, decltype(&Device<pcpp::PcapFileReaderDevice>::deleter)>;
    static std::unique_ptr<IDevice> create(std::string& path);
    virtual std::optional<pcpp::RawPacket> get() override;
protected:
    FileReaderPtr create_reader(std::string& name);
    FileDevice(std::string& path);

};
