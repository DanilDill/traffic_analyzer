#include "ApplicationFacade.h"
#include <SystemUtils.h>
#include "PacketReader.h"
#include <fstream>

ApplicationFacade::ApplicationFacade(int argc, char* argv[])
{
    init(argc, argv);
}

void ApplicationFacade::init(int argc, char* argv[])
{
    pcpp::AppName::init(argc, argv);
    parse_options(argc,argv);
    createInputStream();
}


void ApplicationFacade::run()
{
    for (std::optional<pcpp::RawPacket> rawPacket = _input_stream->get();rawPacket.has_value(); rawPacket = _input_stream->get())
    {

        pcpp::Packet parsedPacket(rawPacket.operator->());
        auto data = PacketReader::get(parsedPacket);
        if (data.has_value())
        {
            auto [src_ip,src_port,dst_ip,dst_port,len] = *data;
            auto key = std::make_pair(src_ip + ":" + std::to_string(src_port),dst_ip + ":" + std::to_string(dst_port));
            auto value = std::make_pair(1,len);
            update_map(std::move(key),std::move(value));
            --_count_packets;
            if (_count_packets == 0)
            {
                break;
            }
        }

    }

    std::ofstream output(_output_filepath);
    output << "host_src" <<";"<<"host_dst" << ";"<< "count packets" << ";" << "bytes" << std::endl;
    for(auto& i: _statistic)
    {
        auto [key, value] = i;
        output <<  key.first << ";" << key.second<<";"<< value.first << ";" << value.second  <<std::endl;
    }
}

void ApplicationFacade::update_map(std::pair<std::string,std::string>&& key, std::pair<uint,uint>&& value)
{
    ++_statistic[key].first;
    _statistic[key].second += value.second;
}

void ApplicationFacade::parse_options(int argc, char* argv[])
{
    int optionIndex = 0;
    int opt = 0;
    while((opt = getopt_long(argc, argv, "i:f:o:p:h", options, &optionIndex)) != -1)
    {
        switch (opt)
        {
            case 0:
                break;
            case 'i':
                _interface_name = optarg;
                break;
            case 'f':
                _input_filepath = optarg;
                break;
            case 'o':
                _output_filepath = optarg;
                break;
            case 'h':
                printUsage();
                exit(0);
                break;
            case 'p':
                _count_packets = std::stoi(optarg);
                break;
            default:
                printUsage();
                exit(-1);
        }
    }

}

void ApplicationFacade::createInputStream()
{
    bool isEmpty = _interface_name.empty() && _input_filepath.empty();
    bool isIncorrect = (!_interface_name.empty() && !_input_filepath.empty());
    if ( isEmpty or isIncorrect)
    {
        throw std::exception(std::runtime_error("Cannot create input device"));
    }

    if (!_interface_name.empty())
    {
        _input_stream = InStream<LiveDevice>::create(_interface_name);
        return;
    }
    if (!_input_filepath.empty())
    {

        _input_stream = InStream<FileDevice>::create(_input_filepath);
        return;
    }
}

void ApplicationFacade::printUsage()
{
    std::cout << std::endl
              << "Usage: PCAP file mode:" << std::endl
              << "----------------------" << std::endl
              << pcpp::AppName::get() << " -f input_file  -o output_file -p packets_count" << std::endl
              << std::endl
              << "Options:" << std::endl
              << std::endl
              << "    -f  input_file         : The input pcap/pcapng file to analyze. Required argument for this mode" << std::endl
              << "    -p  packets_count      : The count of packets for write" << std::endl
              << "    -o  output_file        : The output file" << std::endl
              << "    -h                     : Displays this help message and exits" << std::endl
              << std::endl
              << "Usage: Live traffic mode:" << std::endl
              << "-------------------------" << std::endl
              << pcpp::AppName::get() << "  -i interface -o output_file -p packets_count" << std::endl
              << std::endl
              << "Options:" << std::endl
              << std::endl
              << "    -i interface           : Use the specified interface. Can be interface name (e.g eth0) or interface IPv4 address" << std::endl
              << "    -p  packets_count      : The count of packets for write" << std::endl
              << "    -o  output_file        : The output file" << std::endl
              << "    -h                     : Displays this help message and exits" << std::endl
              << std::endl;
}