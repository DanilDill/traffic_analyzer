#include "LiveDevice.h"

#include <SystemUtils.h>
 std::unique_ptr<IDevice> LiveDevice::create(std::string& name)
{
    try
    {
        return std::unique_ptr<LiveDevice>(new LiveDevice(name));
    }
    catch (const std::exception& ex)
    {
        std::cerr << "What :" << ex.what() << std::endl;
    }
}

std::optional<pcpp::RawPacket> LiveDevice::get()
{
    pcpp::RawPacketVector packetVec;
    while (packetVec.size() == 0)
    {
        _reader->startCapture(packetVec);
        pcpp::multiPlatformSleep(1);
        _reader->stopCapture();
    }


    return *packetVec.front();
};


LiveDevice::LiveDevicePtr LiveDevice::create_reader(std::string& name)
{
    return LiveDevicePtr(pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByName(name), &deleter);
}

LiveDevice::LiveDevice(std::string& name): Device<pcpp::PcapLiveDevice>(name,create_reader(name))
{

}

