#include <tuple>
#include <string>
#include <optional>
#include <IPv4Layer.h>
#include <IPv6Layer.h>
#include <TcpLayer.h>
#include <UdpLayer.h>
#include <Packet.h>
using PacketData = std::tuple<std::string,uint,std::string,uint,size_t>;//srcIP,srcPort,DstIP,DstPort,len
class PacketReader
{
public:
    static std::optional<PacketData> get(pcpp::Packet& packet)
    {

        if (packet.isPacketOfType(pcpp::IPv4))
        {
            if (packet.isPacketOfType(pcpp::TCP))
            {
                return PacketReader::get<pcpp::IPv4Layer,pcpp::TcpLayer>(packet);
            }
            if (packet.isPacketOfType(pcpp::UDP))
            {
                return PacketReader::get<pcpp::IPv4Layer,pcpp::UdpLayer>(packet);
            }

        }
        if (packet.isPacketOfType(pcpp::IPv6))
        {
            if (packet.isPacketOfType(pcpp::TCP))
            {
                return PacketReader::get<pcpp::IPv6Layer,pcpp::TcpLayer>(packet);
            }
            if (packet.isPacketOfType(pcpp::UDP))
            {
                return PacketReader::get<pcpp::IPv6Layer,pcpp::UdpLayer>(packet);
            }
        }
        return std::nullopt;
    }

private:
    template<typename NetworkLayer, typename TransportLayer>
    static PacketData get(pcpp::Packet& packet)
    {
        std::string srcIP;
        std::string destIP;
        if constexpr (std::is_same_v<NetworkLayer, pcpp::IPv4Layer>)
        {
            srcIP = packet.getLayerOfType<NetworkLayer>()->getSrcIPv4Address().toString();
            destIP = packet.getLayerOfType<NetworkLayer>()->getDstIPv4Address().toString();
        } else if constexpr (std::is_same_v<NetworkLayer, pcpp::IPv6Layer>)
        {
            srcIP = packet.getLayerOfType<NetworkLayer>()->getSrcIPv6Address().toString();
            destIP = packet.getLayerOfType<NetworkLayer>()->getDstIPv6Address().toString();
        }

        uint portSrc  = packet.getLayerOfType<TransportLayer>()->getSrcPort();

        uint portDst = packet.getLayerOfType<TransportLayer>()->getDstPort();
        size_t len =  packet.getLayerOfType<NetworkLayer>()->getDataLen();
        return PacketData { srcIP, portSrc, destIP, portDst, len };
    }
};