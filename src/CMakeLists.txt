project(traffic_analyzer)
message(STATUS "    building ${PROJECT_NAME} ")

file(GLOB include ${CMAKE_CURRENT_SOURCE_DIR}/*.h)
file(GLOB sources ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)
include_directories(${PCAP_PLUS_PLUS_INCLUDE})

file(COPY ${PCAP_LIBRARY_DIR}/ DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/lib)
file(COPY ${PcapPlusPlus_ROOT}/lib/ DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/lib)

set(${CMAKE_INSTALL_RPATH_USE_LINK_PATH} TRUE)
set (${CMAKE_BUILD_RPATH} ${CMAKE_CURRENT_BINARY_DIR}/lib)
set(${CMAKE_INSTALL_RPATH} "${CMAKE_INSTALL_PREFIX}/lib/")

add_executable(${PROJECT_NAME} main.cpp ${include} ${sources} )
link_directories(${CMAKE_CURRENT_BINARY_DIR}/lib)
target_link_libraries( ${PROJECT_NAME}
        PUBLIC PcapPlusPlus::Pcap++
        PCAP::PCAP
        )



install(TARGETS ${PROJECT_NAME} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
install( DIRECTORY ${PcapPlusPlus_ROOT}/lib/  DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)
install( DIRECTORY ${PCAP_LIBRARY_DIR}/  DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)
install(DIRECTORY ${ROOT_DIR}/python/ DESTINATION  ${CMAKE_INSTALL_PREFIX}/bin)


