#pragma once
#include <RawPacket.h>
#include <PcapFileDevice.h>

#include <memory>
#include <exception>
#include <iostream>

#include "FileDevice.h"
#include "LiveDevice.h"
class IStream
{
public:
    IStream()= default;
    virtual std::optional<pcpp::RawPacket> get()=0;

};

template<typename InputDevice>
class InStream : public IStream
{
private:
    std::unique_ptr<IDevice> device;
    InStream(std::string& name)
    {
        device = InputDevice::create(name);
    }

public:
    static std::unique_ptr<InStream<InputDevice>> create(std::string& name)
    {
        return std::unique_ptr<InStream<InputDevice>> (new InStream<InputDevice>(name));
    };
    std::optional<pcpp::RawPacket> get() { return device->get();}

};






