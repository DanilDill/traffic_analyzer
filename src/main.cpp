#include "ApplicationFacade.h"
#include "PacketReader.h"
int main(int argc, char* argv[])
{
    ApplicationFacade application(argc,argv);
    application.run();
    return 0;
}