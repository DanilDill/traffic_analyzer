#pragma once
#include <RawPacket.h>
#include <PcapFileDevice.h>
#include <optional>
#include <string>
#include <iostream>

class IDevice
{
public:
    virtual std::optional<pcpp::RawPacket> get()=0;

};


template<typename T>
class Device : public IDevice
{
protected:
    static void deleter(T* device){ device->close();}
    Device();
    Device(std::string& name, std::unique_ptr<T, decltype(&deleter)>&& reader);
    void try_open();
    std::unique_ptr<T, decltype(&deleter)> _reader;
    std::string _name;

public:
    virtual std::optional<pcpp::RawPacket> get(){};

};

template<typename T>
Device<T>::Device():
_reader(nullptr, nullptr), _name("")
{
};

template<typename T>
Device<T>::Device(std::string& name, std::unique_ptr<T, decltype(&deleter)>&& reader): _reader(std::move(reader)),_name(name)
{
    try_open();
};


template<typename T>
void Device<T>::try_open()
{
    if(!_reader->open())
    {
        _reader.reset();
        throw std::runtime_error(" can't open device : " + _name);
    }
}
