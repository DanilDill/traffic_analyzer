#include "Device.h"
#include <PcapLiveDeviceList.h>
class LiveDevice : public Device<pcpp::PcapLiveDevice>
{
public:
    using LiveDevicePtr = std::unique_ptr<pcpp::PcapLiveDevice, decltype(&Device<pcpp::PcapLiveDevice>::deleter)>;
    static std::unique_ptr<IDevice> create(std::string& name);
    virtual std::optional<pcpp::RawPacket> get() override;
    LiveDevicePtr create_reader(std::string& name);
private:
    LiveDevice(std::string& name);

};
